/*
 * controls.cpp
 *
 *  Created on: Jan 13, 2022
 *      Author: fsk
 */

#include "controls.hpp"

namespace cgp {





// update the camera position
void camera_control(GLFWwindow* window, glm::mat4* ViewMatrix, glm::vec3* position)
{
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
	{
		(*position)[2] -= 0.05f;
	}
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
	{
		(*position)[2] += 0.05f;
	}
	if (glfwGetKey( window, GLFW_KEY_R) == GLFW_PRESS){

		(*position)[2] = 10.0f;
		*ViewMatrix = 	glm::lookAt(glm::vec3(0.0f, (*position)[1], (*position)[2]),
						glm::vec3(0.0f, (*position)[1], 0.0f),
						glm::vec3(0.0f, 1.0f, 0.0f));
	}
	else
	{
		*ViewMatrix = 	glm::lookAt(glm::vec3(0.0f, (*position)[1], (*position)[2]),
						glm::vec3(0.0f, (*position)[1], 0.0f),
						glm::vec3(0.0f, 1.0f, 0.0f));
	}
}

// transform the model
void transform_model(GLFWwindow* window, glm::mat4* ModelMatrix, glm::vec3* model_pos, float* rx, float* ry)
{
	*ModelMatrix = glm::translate(*ModelMatrix, *model_pos);
	if (glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS){
		*rx = -1.0f;
	}
	else if (glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
		*rx = 1.0f;
	} else {
		*rx = 0.0f;
	}

	if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS){
		*ry = -1.0f;
	}
	else if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS){
		*ry = 1.0f;
	} else {
		*ry = 0.0f;
	}

	if (glfwGetKey( window, GLFW_KEY_R ) == GLFW_PRESS){

		*ModelMatrix = glm::mat4(1);
	}
	else
	{
		*ModelMatrix = glm::rotate(*ModelMatrix,glm::radians(*ry),glm::vec3(1,0,0));//rotation x = 0.0 degrees
		*ModelMatrix = glm::rotate(*ModelMatrix,glm::radians(*rx),glm::vec3(0,1,0));//rotation y = 0.0 degrees
		*ModelMatrix = glm::rotate(*ModelMatrix,glm::radians(0.0f),glm::vec3(0,0,1));//rotation z = 0.0 degrees
		*ModelMatrix = glm::scale(*ModelMatrix,glm::vec3(1,1,1));//scale = 1,1,1
	}
}


void transfom_light(GLFWwindow* window, glm::vec3* lightPos)
{
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		(*lightPos)[0] += 0.1f;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		(*lightPos)[0] -= 0.1f;
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		(*lightPos)[1] += 0.1f;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		(*lightPos)[1] -= 0.1f;
	}
	if (glfwGetKey( window, GLFW_KEY_R ) == GLFW_PRESS){

		*lightPos = glm::vec3(4.0f,4.0f,4.0f);
	}
}

void change_light_power(GLFWwindow* window, GLfloat* LightPower)
{
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		glUniform1f(*LightPower, 10.0f);
	}
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		glUniform1f(*LightPower, 20.0f);
	}
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
	{
		glUniform1f(*LightPower, 50.0f);
	}
	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
	{
		glUniform1f(*LightPower, 80.0f);
	}
	if (glfwGetKey( window, GLFW_KEY_R ) == GLFW_PRESS){

		glUniform1f(*LightPower, 50.0f);
	}
}

void change_light_color(GLFWwindow* window, GLuint* LightColor, glm::vec3* lightCol)
{
	if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
	{
		lightCol->x = 1;
		lightCol->y = 0;
		lightCol->z = 0;
	}
	if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)
	{
		lightCol->x = 0;
		lightCol->y = 1;
		lightCol->z = 0;
	}
	if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS)
	{
		lightCol->x = 0;
		lightCol->y = 0;
		lightCol->z = 1;
	}
	if (glfwGetKey( window, GLFW_KEY_R ) == GLFW_PRESS){

		lightCol->x = 1;
		lightCol->y = 1;
		lightCol->z = 1;
	}
	if (glfwGetKey( window, GLFW_KEY_C ) == GLFW_PRESS){

		lightCol->x = 1;
		lightCol->y = 1;
		lightCol->z = 1;
	}
	glUniform3f(*LightColor, lightCol->x, lightCol->y, lightCol->z);
}


}  // namespace cgp

