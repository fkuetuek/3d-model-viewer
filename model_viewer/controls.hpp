/*
 * controls.hpp
 *
 *  Created on: Jan 13, 2022
 *      Author: fsk
 */

#ifndef MODEL_VIEWER_CONTROLS_HPP_
#define MODEL_VIEWER_CONTROLS_HPP_

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace cgp {

// update the camera position
void camera_control(GLFWwindow* window, glm::mat4* ViewMatrix, glm::vec3* position);
// transform the model
void transform_model(GLFWwindow* window, glm::mat4* ModelMatrix, glm::vec3* model_pos, float* rx, float* ry);
// transform the light
void transfom_light(GLFWwindow* window, glm::vec3* lightPos);
// adjust the light power
void change_light_power(GLFWwindow* window, GLfloat* LightPower);
// adjust the light color
void change_light_color(GLFWwindow* window, GLuint* LightColor, glm::vec3* lightCol);



}  // namespace cgp


#endif /* MODEL_VIEWER_CONTROLS_HPP_ */
